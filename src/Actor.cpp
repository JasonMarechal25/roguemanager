#include "libtcod.hpp"
#include "Actor.h"
#include "Map.h"
#include "Engine.h"
#include "AI.h"
#include "Destructible.h"
#include "Attacker.h"
#include "GUI.h"
#include "Container.h"
#include "Pickable.h"

Actor::Actor(int x, int y, int ch, std::string name, const TCODColor& col) :
	x{ x },
	y{ y },
	ch{ ch },
	col{ col },
	name{ name } ,
	blocks{ true },
	attacker{ nullptr },
	destructible{ nullptr },
	ai{ nullptr },
	pickable{ nullptr },
	container{ nullptr }
 {

}

void Actor::render() const {
	TCODConsole::root->setChar(x, y, ch);
	TCODConsole::root->setCharForeground(x, y, col);
}

void Actor::update() {
	if (ai) ai->update(*this);
}

bool Actor::moveOrAttack(int x, int y) {
	if (engine.map->isWall(x, y)) return false;
	for (auto actor: engine.actors) {
		if (actor->x == x && actor->y == y) {
			engine.gui->message(TCODColor::lightGrey, "The " + actor->name + " laughs at your puny efforts to attack him!");
			return false;
		}
	}
	this->x = x;
	this->y = y;
	return true;
}

float Actor::getDistance(int cx, int cy) const {
    int dx=x-cx;
    int dy=y-cy;
    return sqrtf(dx*dx+dy*dy);
}

Actor::~Actor() {

}
