#include "GUI.h"
#include "Engine.h"
#include "Actor.h"
#include "Destructible.h"
#include <sstream>
#include <iomanip>
#include "Map.h"

static const int PANEL_HEIGHT = 7;
static const int BAR_WIDTH = 20;

static const int MSG_X = BAR_WIDTH + 2;
static const int MSG_HEIGHT = PANEL_HEIGHT - 1;

GUI::GUI() {
	con = std::make_unique<TCODConsole>(engine.screenWidth, PANEL_HEIGHT);
}

GUI::~GUI() {
}

GUI::Message::Message(std::string text, TCODColor col) :
	col{ col },
	text{ text }{
}

GUI::Message::~Message() {

}


void GUI::render() {
	// clear the GUI console
	con->setDefaultBackground(TCODColor::black);
	con->clear();
	// draw the health bar
	renderBar(1, 1, BAR_WIDTH, "HP", engine.player->destructible->hp,
		engine.player->destructible->maxHp,
		TCODColor::lightRed, TCODColor::darkerRed);

	// draw the message log
	int y = 1;
	float colorCoef = 0.4f;
	for (auto message: log) {
		TCODColor c = message.col * colorCoef;
		con->setDefaultForeground(message.col * colorCoef);
		con->print(MSG_X, y, message.text.c_str());
		y++;
		if (colorCoef < 1.0f) {
			colorCoef += 0.3f;
		}
	}

	// mouse look
	renderMouseLook();

	// blit the GUI console on the root console
	TCODConsole::blit(con.get(), 0, 0, engine.screenWidth, PANEL_HEIGHT,
		TCODConsole::root, 0, engine.screenHeight - PANEL_HEIGHT);
}

void GUI::renderBar(int x, int y, int width, std::string name,
	float value, float maxValue, TCODColor barColor,
	TCODColor backColor) {
	// fill the background
	con->setDefaultBackground(backColor);
	con->rect(x, y, width, 1, false, TCOD_BKGND_SET);
	int barWidth = (int)(value / maxValue * width);
	if (barWidth > 0) {
		// draw the bar
		con->setDefaultBackground(barColor);
		con->rect(x, y, barWidth, 1, false, TCOD_BKGND_SET);
	}
	// print text on top of the bar
	con->setDefaultForeground(TCODColor::white);
	//TCODConsole::root->print(x + width / 2, y, std::string(name + " : " + std::to_string(value) + "/" + std::to_string(maxValue)), TCOD_CENTER, TCOD_BKGND_NONE);
	std::stringstream ss;
	ss << name << " : "
		<< std::setprecision(0) << value << "/" << maxValue;
	con->printf(x + width / 2, y, TCOD_BKGND_NONE, TCOD_CENTER, ss.str().c_str());
}

void GUI::message(TCODColor col, std::string text) {
	size_t lineEnd;
	std::vector<std::string> splitedString;

	do {
		lineEnd = text.find('\n', 0);
		splitedString.emplace_back(text.substr(0, lineEnd));
		text = text.erase(0, lineEnd+1);
	} while (lineEnd != std::string::npos);

	for (auto str : splitedString) {
		if (log.size() == MSG_HEIGHT) {
			log.pop_front();
		}

		// add a new message to the log
		log.emplace_back(str, col);
	}
}

void GUI::renderMouseLook() {
	if (!engine.map->isInFov(engine.mouse.cx, engine.mouse.cy)) {
		// if mouse is out of fov, nothing to render
		return;
	}

	std::string underPointerString;
	bool first = true;
	for (auto actor: engine.actors) {
		// find actors under the mouse cursor
		if (actor->x == engine.mouse.cx && actor->y == engine.mouse.cy) {
			if (!first) {
				underPointerString += ", ";
			}
			else {
				first = false;
			}
			underPointerString += actor->name;
		}
	}
	// display the list of actors under the mouse cursor
	con->setDefaultForeground(TCODColor::lightGrey);
	con->print(1, 0, underPointerString.c_str());
}