#pragma once
#include "libtcod.hpp"
#include <deque>

class GUI {
public:
	GUI();
	~GUI();
	void render();

protected:
	std::unique_ptr<TCODConsole> con;
	struct Message {
		const std::string text;
		TCODColor col;
		Message(std::string text, TCODColor col);
		~Message();
	};
	std::deque<Message> log;

	void renderBar(int x, int y, int width, std::string name,
		float value, float maxValue, TCODColor barColor,
		TCODColor backColor);

	void renderMouseLook();
public:
	void message(TCODColor col, std::string text);
};