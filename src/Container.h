//
// Created by Archfiend on 08/11/2019.
//

#pragma once
#ifndef ROGUEMANAGER_CONTAINER_H
#define ROGUEMANAGER_CONTAINER_H

#include "libtcod.hpp"

class Actor;

class Container {
public :
    int size; // maximum number of actors. 0=unlimited
    std::vector<std::unique_ptr<Actor>> inventory;

    Container(int size);
    ~Container();
    bool add(Actor& actor);
    void remove(Actor &actor);
};


#endif //ROGUEMANAGER_CONTAINER_H
