﻿// main.cpp : définit le point d'entrée de l'application.
//

#include <iostream>
#include "libtcod.hpp"
#include "Engine.h"

// Hide the console window
//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

Engine engine(80, 50);

int main() {
	while (!TCODConsole::isWindowClosed()) {
		engine.update();
		engine.render();
		TCODConsole::flush();
	}
	return 0;
}
