#pragma once

#include <string>

class Actor;

class Destructible {
public:
	float maxHp; // maximum health points
	float hp; // current health points
	float defense; // hit points deflected
	const std::string corpseName; // the actor's name once dead/destroyed

	Destructible(float maxHp, float defense, std::string corpseName);
	virtual ~Destructible();

	inline bool isDead() { return hp <= 0; }

	float takeDamage(Actor& owner, float damage);
	float heal(float amount);

	virtual void die(Actor& owner);
};

class MonsterDestructible : public Destructible {
public:
	MonsterDestructible(float maxHp, float defense, std::string corpseName);
	~MonsterDestructible();
	
	void die(Actor& owner);
};

class PlayerDestructible : public Destructible {
public:
	PlayerDestructible(float maxHp, float defense, std::string corpseName);
	~PlayerDestructible();

	void die(Actor& owner);
};