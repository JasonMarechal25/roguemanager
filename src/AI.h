#pragma once

class Actor;

class AI {
public:
	virtual void update(Actor& owner) = 0;
};

class PlayerAi : public AI {
public:
	void update(Actor& owner);

protected:
	bool moveOrAttack(Actor& owner, int targetx, int targety);
    void handleActionKey(Actor &owner, int ascii);
    Actor* choseFromInventory(Actor &owner);


};

class MonsterAi : public AI {
public:
	void update(Actor& owner);

protected:
	void moveOrAttack(Actor& owner, int targetx, int targety);

protected:
	int moveCount;
};