//
// Created by Archfiend on 08/11/2019.
//

#pragma once
#ifndef ROGUEMANAGER_PICKABLE_H
#define ROGUEMANAGER_PICKABLE_H

#include <memory>

class Actor;

class Pickable {
public :
    bool pick(Actor &owner, Actor &wearer);
    virtual bool use(Actor& owner, Actor &wearer);
};

class Healer : public Pickable {
public :
    float amount; // how many hp

    Healer(float amount);
    bool use(Actor &owner, Actor &wearer);
};

class LightningBolt: public Pickable {
public :
    float range,damage;
    LightningBolt(float range, float damage);
    bool use(Actor &owner, Actor &wearer);
};

class Fireball : public LightningBolt {
public :
    Fireball(float range, float damage);
    bool use(Actor &owner, Actor &wearer);
};

#endif //ROGUEMANAGER_PICKABLE_H
