//
// Created by Archfiend on 08/11/2019.
//

#pragma once

#include "Container.h"
#include "Actor.h"

Container::Container(int size):
    size{size}
    {

}

Container::~Container() {

}

bool Container::add(Actor& actor) {
    if ( size > 0 && inventory.size() >= size ) {
        // inventory full
        return false;
    }
    inventory.emplace_back(&actor);
    return true;
}

void Container::remove(Actor &actor) {
    const auto& toRemove = std::find_if(inventory.begin(), inventory.end(), [&](auto & content){
        return &actor == content.get();
    });
    if (toRemove != inventory.end())
        inventory.erase(toRemove);
    /*
     * else Assert(not found) //TODO
     */
}
