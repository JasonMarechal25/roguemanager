#include "Destructible.h"
#include "Actor.h"
#include "libtcod.h"
#include "Engine.h"
#include "GUI.h"

#include <algorithm>

Destructible::Destructible(float maxHp, float defense, std::string corpseName) :
	maxHp{ maxHp },
	hp{ maxHp },
	defense{ defense },
	corpseName{ corpseName } {
}

Destructible::~Destructible()
{
}


float Destructible::takeDamage(Actor& owner, float damage) {
	damage -= defense;
	if (damage > 0) {
		hp -= damage;
		if (hp <= 0) {
			die(owner);
		}
	}
	else {
		damage = 0;
	}
	return damage;
}

float Destructible::heal(float amount) {
	int oldhp = hp;
	hp = std::min(hp + amount, maxHp);
	return std::min(amount, maxHp-oldhp);
}

void Destructible::die(Actor& owner) {
	// transform the actor into a corpse!
	owner.ch = '%';
	owner.col = TCODColor::darkRed;
	owner.name = corpseName;
	owner.blocks = false;
	// make sure corpses are drawn before living actors
	engine.sendToBack(owner);
}

MonsterDestructible::MonsterDestructible(float maxHp, float defense, std::string corpseName) :
	Destructible(maxHp, defense, corpseName) {
}

PlayerDestructible::PlayerDestructible(float maxHp, float defense, std::string corpseName) :
	Destructible(maxHp, defense, corpseName) {
}

PlayerDestructible::~PlayerDestructible()
{
}

void MonsterDestructible::die(Actor& owner) {
	// transform it into a nasty corpse! it doesn't block, can't be
	// attacked and doesn't move
	engine.gui->message(TCODColor::lighterGrey, owner.name + " is dead");
	Destructible::die(owner);
}

MonsterDestructible::~MonsterDestructible()
{
}

void PlayerDestructible::die(Actor& owner) {
	engine.gui->message(TCODColor::red, "You died!");
	Destructible::die(owner);
	engine.gameStatus = Engine::GameStatus::DEFEAT;
}