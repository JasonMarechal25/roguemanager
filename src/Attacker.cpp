#include <stdio.h>
#include "Attacker.h"
#include "Actor.h"
#include "Destructible.h"
#include "Engine.h"
#include "GUI.h"

Attacker::Attacker(float power) :
	power{ power } {
}

void Attacker::attack(Actor& owner, Actor& target) {
	if (target.destructible && !target.destructible->isDead()) {
		int dammage = power - target.destructible->defense;
		if (power - target.destructible->defense > 0) {
			engine.gui->message(&owner == engine.player ? TCODColor::red : TCODColor::lightGrey,
				owner.name + " attacks " + target.name + " for " + std::to_string(dammage) + " hit points.");
		}
		else {
			engine.gui->message(TCODColor::lightGrey, owner.name + " attacks " + target.name + " but it has no effect!");
		}
		target.destructible->takeDamage(target, power);
	}
	else {
		engine.gui->message(TCODColor::lightGrey, owner.name + " attacks " + target.name + " in vain.");
	}
}