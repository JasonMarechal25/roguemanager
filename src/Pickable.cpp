//
// Created by Archfiend on 08/11/2019.
//

#pragma once

#include <sstream>
#include <iomanip>
#include "Pickable.h"
#include "Engine.h"
#include "Actor.h"
#include "Container.h"
#include "Destructible.h"
#include "libtcod.hpp"
#include "GUI.h"

bool Pickable::pick(Actor &owner, Actor &wearer) {
    if (wearer.container && wearer.container->add(owner)) {
        engine.actors.remove(&owner);
        return true;
    }
    return false;
}

bool Pickable::use(Actor &owner, Actor &wearer) {
    if (wearer.container) {
        wearer.container->remove(owner);
        return true;
    }
    return false;
}

Healer::Healer(float amount) :
        amount{amount} {

}

bool Healer::use(Actor &owner, Actor &wearer) {
    if (wearer.destructible) {
        float amountHealed = wearer.destructible->heal(amount);
        if (amountHealed > 0) {
            return Pickable::use(owner, wearer);
        }
    }
    return false;
}

LightningBolt::LightningBolt(float range, float damage) :
        range{range},
        damage{damage} {

}

bool LightningBolt::use(Actor &owner, Actor &wearer) {
    Actor *closestMonster = engine.getClosestMonster(wearer.x, wearer.y, range);
    if (!closestMonster) {
        engine.gui->message(TCODColor::lightGrey, "No enemy is close enough to strike.");
        return false;
    }
    // hit closest monster for <damage> hit points
    std::stringstream ss;
    ss << "A lighting bolt strikes the " << closestMonster->name << " with a loud thunder!\nThe damage is "
       << std::setprecision(0) << damage << " hit points.";
    engine.gui->message(TCODColor::lightBlue,
                        ss.str());
    closestMonster->destructible->takeDamage(*closestMonster, damage);
    return Pickable::use(owner, wearer);
}

Fireball::Fireball(float range, float damage) : LightningBolt(range, damage) {

}

bool Fireball::use(Actor &owner, Actor &wearer) {
    engine.gui->message(TCODColor::cyan, "Left-click a target tile for the fireball,\nor right-click to cancel.");
    int x, y;
    if (!engine.pickATile(x, y)) {
        return false;
    }
    // burn everything in <range> (including player)
    engine.gui->message(TCODColor::orange,
                        "The fireball explodes, burning everything within " + std::to_string(range) + " tiles!");
    for (auto actor: engine.actors) {
        if (actor->destructible && !actor->destructible->isDead()
            && actor->getDistance(x, y) <= range) {
            std::stringstream ss;
            ss << "The " << actor->name << " gets burned for " << damage << " hit points.";
            engine.gui->message(TCODColor::orange, ss.str());
            actor->destructible->takeDamage(*actor, damage);
        }
    }
    return Pickable::use(owner,wearer);
}

