#pragma once

#include "libtcod.h"

class Actor;
class Map;
class GUI;

class Engine {
public:
	enum class GameStatus {
		STARTUP,
		IDLE,
		NEW_TURN,
		VICTORY,
		DEFEAT
	} gameStatus;

public:
	TCODList<Actor*> actors;
	Actor* player;
	std::unique_ptr<Map> map;

	Engine(int screenWidth, int screenHeight);
	~Engine();
	void update();
	void render();
	void sendToBack(Actor& actor);
    Actor *getClosestMonster(int x, int y, float range) const;
    bool pickATile(int &x, int &y, float maxRange = 0.0f);

public:
	int fovRadius;
	int screenWidth;
	int screenHeight;
	std::unique_ptr<GUI> gui;
	TCOD_key_t lastKey;
	TCOD_mouse_t mouse;

private:
	bool computeFov;
};

extern Engine engine;