#include "libtcod.hpp"
#include "Map.h"
#include "Actor.h"
#include "Engine.h"
#include "Destructible.h"
#include "AI.h"
#include "Attacker.h"
#include "Pickable.h"

constexpr int ROOM_MAX_SIZE = 12;
constexpr int ROOM_MIN_SIZE = 6;
constexpr int MAX_ROOM_MONSTERS = 3;
constexpr int MAX_ROOM_ITEMS = 2;

class BspListener : public ITCODBspCallback {
private:
    Map &map; // a map to dig
    int roomNum; // room number
    int lastx, lasty; // center of the last room
public:
    BspListener(Map &map)
            : map{map}, roomNum{0}, lastx{0}, lasty{0} {}

    bool visitNode(TCODBsp *node, void *userData) {
        if (node->isLeaf()) {
            int x, y, w, h;
            // dig a room
            TCODRandom *rng = TCODRandom::getInstance();
            w = rng->getInt(ROOM_MIN_SIZE, node->w - 2);
            h = rng->getInt(ROOM_MIN_SIZE, node->h - 2);
            x = rng->getInt(node->x + 1, node->x + node->w - w - 1);
            y = rng->getInt(node->y + 1, node->y + node->h - h - 1);
            map.createRoom(roomNum == 0, x, y, x + w - 1, y + h - 1);

            if (roomNum != 0) {
                // dig a corridor from last room
                map.dig(lastx, lasty, x + w / 2, lasty);
                map.dig(x + w / 2, lasty, x + w / 2, y + h / 2);
            }

            lastx = x + w / 2;
            lasty = y + h / 2;
            roomNum++;
        }
        return true;
    }
};

Map::Map(int width, int height)
        : width{width}, height{height}, tiles(width * height), map{std::make_unique<TCODMap>(width, height)} {
    TCODBsp bsp(0, 0, width, height);
    bsp.splitRecursive(nullptr, 8, ROOM_MAX_SIZE, ROOM_MAX_SIZE, 1.5f, 1.5f);
    BspListener listener(*this);
    bsp.traverseInvertedLevelOrder(&listener, nullptr);
}

Map::~Map() {

}

bool Map::isWall(int x, int y) const {
    return !map->isWalkable(x, y);
}

bool Map::isExplored(int x, int y) const {
    return tiles[x + y * width].explored;
}

bool Map::isInFov(int x, int y) const {
    if (x < 0 || x >= width || y < 0 || y >= height) {
        return false;
    }
    if (map->isInFov(x, y)) {
        tiles[x + y * width].explored = true;
        return true;
    }
    return false;
}

void Map::computeFov() {
    map->computeFov(engine.player->x, engine.player->y, engine.fovRadius);
}

void Map::render() const {
    static const TCODColor darkWall(0, 0, 100);
    static const TCODColor darkGround(50, 50, 150);

    static const TCODColor lightWall(130, 110, 50);
    static const TCODColor lightGround(200, 180, 50);

    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            if (isInFov(x, y)) {
                TCODConsole::root->setCharBackground(x, y,
                                                     isWall(x, y) ? lightWall : lightGround);
            } else if (isExplored(x, y)) {
                TCODConsole::root->setCharBackground(x, y,
                                                     isWall(x, y) ? darkWall : darkGround);
            }
        }
    }
}

void Map::dig(int x1, int y1, int x2, int y2) {
    if (x2 < x1) {
        std::swap(x1, x2);
    }
    if (y2 < y1) {
        std::swap(y1, y2);
    }
    for (int tilex = x1; tilex <= x2; tilex++) {
        for (int tiley = y1; tiley <= y2; tiley++) {
            map->setProperties(tilex, tiley, true, true);
        }
    }
}

void Map::createRoom(bool first, int x1, int y1, int x2, int y2) {
    dig(x1, y1, x2, y2);
    if (first) {
        // put the player in the first room
        engine.player->x = (x1 + x2) / 2;
        engine.player->y = (y1 + y2) / 2;
    } else {
        TCODRandom *rng = TCODRandom::getInstance();
        int nbMonsters = rng->getInt(0, MAX_ROOM_MONSTERS);
        while (nbMonsters > 0) {
            int x = rng->getInt(x1, x2);
            int y = rng->getInt(y1, y2);
            if (canWalk(x, y)) {
                addMonster(x, y);
            }
            nbMonsters--;
        }
        // add items
        int nbItems = rng->getInt(0, MAX_ROOM_ITEMS);
        while (nbItems > 0) {
            int x = rng->getInt(x1, x2);
            int y = rng->getInt(y1, y2);
            if (canWalk(x, y)) {
                addItem(x, y);
            }
            nbItems--;
        }
    }
}

bool Map::canWalk(int x, int y) const {
    if (isWall(x, y)) {
        // this is a wall
        return false;
    }
    for (auto actor: engine.actors) {
        if (actor->blocks && actor->x == x && actor->y == y) {
            // there is an actor there. cannot walk
            return false;
        }
    }
    return true;
}

void Map::addMonster(int x, int y) {
    TCODRandom *rng = TCODRandom::getInstance();
    if (rng->getInt(0, 100) < 80) {
        // create an orc
        Actor *orc = new Actor(x, y, 'o', "orc",
                               TCODColor::desaturatedGreen);
        orc->destructible = std::make_unique<MonsterDestructible>(10.f, 0.f, "dead orc");
        orc->attacker = std::make_unique<Attacker>(3.f);
        orc->ai = std::make_unique<MonsterAi>();
        engine.actors.push(orc);
    } else {
        // create a troll
        Actor *troll = new Actor(x, y, 'T', "troll",
                                 TCODColor::darkerGreen);
        troll->destructible = std::make_unique<MonsterDestructible>(16.f, 1.f, "troll carcass");
        troll->attacker = std::make_unique<Attacker>(4.f);
        troll->ai = std::make_unique<MonsterAi>();
        engine.actors.push(troll);
    }
}

void Map::addItem(int x, int y) {
    TCODRandom *rng = TCODRandom::getInstance();
    int dice = rng->getInt(0, 100);
    if (dice < 70) {
        // create a health potion
        Actor *healthPotion = new Actor(x, y, '!', "health potion",
                                        TCODColor::violet);
        healthPotion->blocks = false;
        healthPotion->pickable = std::make_unique<Healer>(4.f);
        engine.actors.push(healthPotion);
    } else if (dice < 70 + 10) {
        // create a scroll of lightning bolt
        Actor *scrollOfLightningBolt = new Actor(x, y, '#', "scroll of lightning bolt",
                                                 TCODColor::lightYellow);
        scrollOfLightningBolt->blocks = false;
        scrollOfLightningBolt->pickable = std::make_unique<LightningBolt>(5.f, 20.f);
        engine.actors.push(scrollOfLightningBolt);
    } else if ( dice < 70+10+10 ) {
        // create a scroll of fireball
        Actor *scrollOfFireball = new Actor(x, y, '#', "scroll of fireball",
                                            TCODColor::lightYellow);
        scrollOfFireball->blocks = false;
        scrollOfFireball->pickable = std::make_unique<Fireball>(3.f, 12.f);
        engine.actors.push(scrollOfFireball);
    }
}
