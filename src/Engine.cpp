#include "libtcod.hpp"
#include "Actor.h"
#include "Map.h"
#include "Engine.h"
#include "Destructible.h"
#include "Attacker.h"
#include "AI.h"
#include "GUI.h"
#include "Container.h"

Engine::Engine(int screenWidth, int screenHeight) :
	gameStatus{ Engine::GameStatus::STARTUP },
	fovRadius{ 10 },
	screenWidth{ screenWidth },
	screenHeight{ screenHeight } {
	TCODConsole::initRoot(screenWidth, screenHeight, "libtcod C++ tutorial", false, TCOD_RENDERER_SDL2);
	player = new Actor(40, 25, '@', "player", TCODColor::white);
	player->destructible = std::make_unique<PlayerDestructible>(30.f, 2.f, "your cadaver");
	player->attacker = std::make_unique<Attacker>(5.f);
    player->container = std::make_unique<Container>(26);
	player->ai = std::make_unique<PlayerAi>();
	actors.push(player);
	map = std::make_unique<Map>(80, 45);
	gui = std::make_unique <GUI>();
	gui->message(TCODColor::red,
		"Welcome stranger!\nPrepare to perish in the Tombs of the Ancient Kings.");
}

Engine::~Engine() {
	actors.clearAndDelete();
}

void Engine::update() {
	if (gameStatus == Engine::GameStatus::STARTUP) map->computeFov();
	gameStatus = Engine::GameStatus::IDLE;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS | TCOD_EVENT_MOUSE, &lastKey, &mouse);;
	player->update();
	if (gameStatus == Engine::GameStatus::NEW_TURN) {
		for (auto actor : actors) {
			if (actor != player) {
				actor->update();
			}
		}
	}
}

void Engine::render() {
	TCODConsole::root->clear();
	// draw the map
	map->render();

	// draw the actors
    for (auto actor: actors) {
		if (map->isInFov(actor->x, actor->y)) {
			actor->render();
		}
    }

	player->render();
	// show the player's stats
	gui->render();
}

void Engine::sendToBack(Actor& actor) {
	actors.remove(&actor);
	actors.insertBefore(&actor, 0);
}

Actor *Engine::getClosestMonster(int x, int y, float range) const {
    Actor *closest=nullptr;
    float bestDistance=1E6f;
    for (auto actor: actors) {
        if ( actor != player && actor->destructible
             && !actor->destructible->isDead() ) {
            float distance=actor->getDistance(x,y);
            if ( distance < bestDistance && ( distance <= range || range == 0.0f ) ) {
                bestDistance=distance;
                closest=actor;
            }
        }
    }
    return closest;
}

bool Engine::pickATile(int &x, int &y, float maxRange) {
    while ( !TCODConsole::isWindowClosed() ) {
        render();
        // highlight the possible range
        for (int cx=0; cx < map->width; cx++) {
            for (int cy=0; cy < map->height; cy++) {
                if ( map->isInFov(cx,cy)
                     && ( maxRange == 0 || player->getDistance(cx,cy) <= maxRange) ) {
                    TCODColor col=TCODConsole::root->getCharBackground(cx,cy);
                    col = col * 1.2f;
                    TCODConsole::root->setCharBackground(cx,cy,col);
                }
            }
        }
        if ( map->isInFov(mouse.cx,mouse.cy)
             && ( maxRange == 0 || player->getDistance(mouse.cx,mouse.cy) <= maxRange )) {
            TCODConsole::root->setCharBackground(mouse.cx,mouse.cy,TCODColor::white);
            if ( mouse.lbutton_pressed ) {
                x=mouse.cx;
                y=mouse.cy;
                return true;
            }
        }
        if (mouse.rbutton_pressed || lastKey.vk != TCODK_NONE) {
            return false;
        }
        TCODConsole::flush();
    }
    return false;
}
