#pragma once

#include "libtcod.hpp"

class Attacker;
class Destructible;
class AI;
class Pickable;
class Container;

class Actor {
public:
	int x, y; // position on map
	int ch; // ascii code
	TCODColor col; // color
	bool blocks; // can we walk on this actor?
	std::unique_ptr<Attacker> attacker; // something that deals damage
	std::unique_ptr<Destructible> destructible; // something that can be damaged
	std::unique_ptr<AI> ai; // something self-updating
	std::unique_ptr<Pickable> pickable; // something that can be picked and used
	std::unique_ptr<Container> container; // something that can contain actors

	std::string name; // the actor's name

	Actor(int x, int y, int ch, std::string name, const TCODColor& col);

    float getDistance(int cx, int cy) const;
    ~Actor();

    void update();
	bool moveOrAttack(int x, int y);
	void render() const;
};