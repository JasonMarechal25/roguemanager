﻿# CMakeList.txt : fichier projet CMake de niveau supérieur, effectuez une configuration globale
# et incluez les sous-projets ici.
#
cmake_minimum_required(VERSION 3.15)
project(RogueManager)

set(CMAKE_CXX_STANDARD 14)

# Incluez les sous-projets.
add_subdirectory (src)